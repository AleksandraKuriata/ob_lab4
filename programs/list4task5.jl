#Aleksandra Kuriata
include("./methods.jl")
using methods
using Gadfly

function task5()
  data = Array[[x->e^x, 0e0, 1e0], [x->x^2*sin(x), -1e0, 1e0]]
  n = [5, 10, 15]

  for i in 1:length(data)
    v = data[i]
    for k in n
      draw(SVG("$(i)$(k).svg", 4inch, 3inch), rysujNnfx(v[1], v[2], v[3], k))
    end
  end
end
task5()



