﻿#Aleksandra Kuriata
using Polynomials

function naturalna(x::Vector{Float64},fx::Vector{Float64})
    a = Poly(fx[length(fx)])

    for i= length(fx)-1:-1:1
        a=Poly(fx[i]) + poly([x[i]])*a      
    end
    println("a-vector of length n + 1 containing the computed coefficients of the natural form = $(a)")
    return Poly(a)
end

function task3()
	naturalna([-1.0,0.0,1.0,2.0], [-1.0,1.0,-1.0,1.0])
end
task3()
