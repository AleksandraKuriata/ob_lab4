#Aleksandra Kuriata

function ilorazyRoznicowe(x::Vector{Float64}, f::Vector{Float64})
  f = copy(f)
  fx = Array(Float64, length(x))

  for i in 1:length(x)
    fx[i] = f[1]
    #println("fx[$(i)] = $(f[1])")
    for a in 1:length(x)-i
      f[a] = (f[a+1] - f[a])/(x[a+i]-x[a])
      #println("fa[$(a)] = $(f[a])")
    end
  end
  println("fx to wektor dlugosci n+1 zawierajacy obliczone ilorazy roznicowe = $(fx)")
  return fx
end

function warNewton(x::Vector{Float64}, fx::Vector{Float64}, t::Float64)
  nt = fx[length(fx)]
  for i in length(fx)-1:-1:1
    nt = fx[i] + (t-x[i])*nt
  end
   println("wartosc wielomianu w pukcie t = $(nt)")
   return nt
end

function task2()
	warNewton(float([3,1,5,6]),ilorazyRoznicowe(float([3,1,5,6]), float([1,-3,2,4])),3e0)
end
task2()
