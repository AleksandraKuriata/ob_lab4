#Aleksandra Kuriata
include("./methods.jl")
using methods
using Gadfly

function task6()
  data = Array[[x->abs(x), -1e0, 1e0], [x->1/(1+x^2), -5e0, 5e0]]
  n = [5, 10, 15]

  for i in 1:length(data)
    v = data[i]
    for k in n
      draw(SVG("$(i)$(k).svg", 4inch, 3inch), rysujNnfx(v[1], v[2], v[3], k))
    end
  end
end
task6()