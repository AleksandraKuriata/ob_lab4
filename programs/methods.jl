#Aleksandra Kuriata
module methods
    export ilorazyRoznicowe
    export warNewton
    export naturalna
    export createDataPoints
    export rysujNnfx

function ilorazyRoznicowe(x::Vector{Float64}, f::Vector{Float64})
  f = copy(f)
  fx = Array(Float64, length(x))

  for i in 1:length(x)
    fx[i] = f[1]
    #println("fx[$(i)] = $(f[1])")
    for a in 1:length(x)-i
      f[a] = (f[a+1] - f[a])/(x[a+i]-x[a])
      #println("fa[$(a)] = $(f[a])")
    end
  end
  println("fx to wektor dlugosci n+1 zawierajacy obliczone ilorazy roznicowe = $(fx)")
  return fx
end

function warNewton(x::Vector{Float64}, fx::Vector{Float64}, t::Float64)
  nt = fx[length(fx)]
  for i in length(fx)-1:-1:1
    nt = fx[i] + (t-x[i])*nt
  end
   println("the value of the polynomial at point  = $(nt)")
   return nt
end

function naturalna(x::Vector{Float64},fx::Vector{Float64})
    a = Poly(fx[length(fx)])

    for i= length(fx)-1:-1:1
        #Poly([1, 2]) <=> Poly(1 + 2�x)
        #poly([1, 2, 3]) <=> (x - 1)(x - 2)(x - 3)
        a=Poly(fx[i]) + poly([x[i]])*a
    end
    println("a-vector of length n + 1 containing the computed coefficients of the natural form  =  $(a)")
    return Poly(a)
end

polynomialPointsMultiplier = 10

function createDataPoints(a::Float64, b::Float64, n::Int)
  h= (b-a)/n
  x = Array(Float64, n+1)
  for k in 0:n
    x[k+1] = a + k*h
  end
  return x
end


function rysujNnfx(f,a::Float64,b::Float64,n::Int)
  c = colorant"red"
  dataPoints = createDataPoints(a, b, n)

  valuesAtPoints = map(x -> f(x), dataPoints)
  fx = ilorazyRoznicowe(dataPoints, valuesAtPoints);

  polynomialPoints = createDataPoints(a, b, polynomialPointsMultiplier * n)
  interpolatedValues = map(k -> warNewton(dataPoints, fx, k), polynomialPoints)

  plot(
    layer(x = polynomialPoints, y = interpolatedValues, Geom.line, Theme(default_color=c)),
    layer(x = dataPoints, y = valuesAtPoints, Geom.point, Theme(default_color=c)),
    layer(f, a, b)
  )
end