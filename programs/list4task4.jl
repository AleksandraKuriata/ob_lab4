#Aleksandra Kuriata
include("./methods.jl")
using methods
using Gadfly

function task4()
 data = Array[[x->x^2, -4e0, 4e0], [x->sin(x), -4e0, 4e0]]
  n = [5, 10, 15]

  for i in 1:length(data)
    v = data[i]
    for k in n
      draw(SVG("function$(i)$(k).svg", 4inch, 3inch), rysujNnfx(v[1], v[2], v[3], k))
    end
  end
end
task4()





