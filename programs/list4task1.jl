#Aleksandra Kuriata

function ilorazyRoznicowe(x::Vector{Float64}, f::Vector{Float64})
    n = length(x)-1
    fx = Array{Float64}(n+1)
    for a in 0:n
        fx[a+1] = f[a+1]
    end
    for i in 1:n.
        for a in n:-1:i
            fx[a+1] = (fx[a+1]-fx[a])/(x[a+1]-x[a-i+1])
        end
    end
    println(fx)
    return fx
end

function task1()
	ilorazyRoznicowe([-1.0,0.0,1.0,2.0], [-1.0,0.0,-1.0,2.0])
end
task1()

function ilorazyRoznicowe(x::Vector{Float64}, f::Vector{Float64})
    n = length(x)-1
    d = Array{Float64}(n+1)
    for i in 0:n
        d[i+1] = f[i+1]
    end
    for j in 1:n
        for i in n:-1:j
            d[i+1] = (d[i+1]-d[i])/(x[i+1]-x[i-j+1])
        end
    end
    return d
end